object Calculations {
    fun positionGeometricCenter(point: Array<Point2D>): Point2D {
        val X = point.sumByDouble { it.x }
        val Y = point.sumByDouble { it.y }

        return Point2D(X / point.size, Y / point.size)
    }

    fun positionCenterOfMass(materialPoint: Array<MaterialPoint2D>): Point2D {
        val x = materialPoint.sumByDouble { it.x * it.mass }
        val y = materialPoint.sumByDouble { it.y * it.mass }
        val mass = materialPoint.sumByDouble { it.mass }

        return MaterialPoint2D(x / mass, y / mass, mass)
    }
}
