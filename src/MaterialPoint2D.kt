class MaterialPoint2D(x: Double, y: Double, val mass: Double) : Point2D(x, y) {

    override fun toString(): String {
        return "MaterialPoint2D{" +
                "x=" + super.x +
                "y" + super.y +
                "mass=" + mass +
                '}'.toString()
    }
}
